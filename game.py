from __future__ import annotations
import requests as rq

import random

from typing import TypeVar

Self = TypeVar('Self', bound='MMBot')


class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"
    SINGLETON = True
    
    def __init__(self: Self, name: str):
        if MMBot.SINGLETON:
            MMBot.SINGLETON = False
            self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']

    def setup_game(self: Self):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))
        self.attempts = 0
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)
        self.choices = [w for w in MMBot.words if is_unique(w)]
        random.shuffle(self.choices)

    def play(self: Self) -> dict:
        
        def post(choice: str) -> tuple[int, str]:
            count=0
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            matches = (rj["feedback"])
            for char in matches:
             if char == 'G':
                count += 1
            
            return count , matches

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        matches, feed = post(choice)
        

        
        tries = [f'{choice}:{matches}']
        Max_attempts = 6
        ctr = 0
        while  Max_attempts>0:
            ctr+=1
            Max_attempts-=1
            if DEBUG:
                print(choice, matches, self.choices[:10])
            self.update(choice, matches)
            if not self.choices:
             break  
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            matches, feed = post(choice)
            tries.append(f'{choice}:{matches}')
        return {"Secret": choice,
                "Attempts": ctr,
                "Feedback": feed,
                "Route": " => ".join(feed),
                "Route": " => ".join(tries)}

    def update(self: Self, choice: str, matches: int):
        def common(choice: str, word: str):
            return len(set(choice) & set(word))
        self.choices = [w for w in self.choices if common(choice, w) == matches]
       

DEBUG = False
bot = MMBot("CodeShifu")
for _ in range(10):
    bot.setup_game()
    print(bot.play())
