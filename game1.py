import requests as rq
import random

DEBUG = False

GREEN, YELLOW, RED = "G", "Y", "R"
SPACE = " "
GAME_WON = "GGGGG"

class SimpleWordleBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    create_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        self.session = rq.session()
        self.me = self.register(name)
        self.create_game()
        self.choices = [word for word in SimpleWordleBot.words if len(set(word)) == len(word)]
        random.shuffle(self.choices)

    def register(self, name: str) -> str:
        response = self.session.post(self.register_url, json={"mode": "wordle", "name": name})
        return response.json()['id']

    def create_game(self):
        self.session.post(self.create_url, json={"id": self.me, "overwrite": True})

    def make_guess(self, choice: str) -> str:
        response = self.session.post(self.guess_url, json={"id": self.me, "guess": choice})
        return response.json()["feedback"]

    def update_choices(self, choice: str, feedback: str):
        greens = [i for i, ch in enumerate(feedback) if ch == GREEN]
        reds = [choice[i] for i, ch in enumerate(feedback) if ch == RED]
        yellows = [choice[i] for i, ch in enumerate(feedback) if ch == YELLOW]

        def matches_feedback(word):
            return all(word[i] == choice[i] for i in greens) and \
                   all(letter not in word for letter in reds) and \
                   all(letter in word for letter in yellows)

        self.choices = [word for word in self.choices if matches_feedback(word)]

    def play(self):
        attempts = []
        choice = random.choice(self.choices)
        self.choices.remove(choice)

        feedback = self.make_guess(choice)
        attempts.append(f'{choice}:{feedback}')

        while feedback != GAME_WON:
            if DEBUG:
                print(f'Choice: {choice}, Feedback: {feedback}, Remaining: {self.choices[:10]}')
            self.update_choices(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback = self.make_guess(choice)
            attempts.append(f'{choice}:{feedback}')

        print(f"Secret word found: {choice} in {len(attempts)} attempts")
        print(f"Attempts: {' => '.join(attempts)}")
game = SimpleWordleBot("CodeShifu")
game.play()
